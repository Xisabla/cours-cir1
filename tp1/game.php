<?php

$debug = false;

session_start();

$size = [20, 50];

// Get parameters (options)
if(isset($_GET)) {
    if(isset($_GET['debug'])) {$debug = true;}
    if(isset($_GET['auto'])) {$sAuto = htmlspecialchars($_GET['auto'], ENT_QUOTES);}
    if(isset($_GET['reset'])) {unset($_SESSION['cells']);}
    if(isset($_GET['generation'])) {$generation = htmlspecialchars($_GET['generation'], ENT_QUOTES);}
}

/**
 * Initialize cells grid
 *
 * @param array $cells  Cells grid
 * @param array $size   Grid size table [x, y]
 * @param bool  $randomize  If true, will generate alive cells randomly
 * @return array    The fresh initialized cells grid
 */
function init($cells, $size, $randomize = true) {
    for($x = 0; $x < $size[0]; $x++) {
        $cells[$x] = Array();
        for($y = 0; $y < $size[1]; $y++) {
            $cells[$x][$y] = ($randomize && (bool) rand(0,1)) ? true : false;
        }
    }

    return $cells;
}

/**
 * Get the number of alive cells around a cell (adjacent only)
 *
 * @param array $cells  Cells grid
 * @param array $size   Grid size table [x, y]
 * @param array $cell   Cell being tested
 * @return int  Number of neighbours
 */
function getNeighbours($cells, $size, $cell) {
    $x = $cell[0];
    $y = $cell[1];

    $neighbours = 0;

    if($x != 0 && $y != 0) {if($cells[$x-1][$y-1]) {$neighbours++;}}
    if($x != 0) {if($cells[$x-1][$y]) {$neighbours++;}}
    if($x != 0 && $y != $size[1]-1) {if($cells[$x-1][$y+1]) {$neighbours++;}}
    if($y != 0) {if($cells[$x][$y-1]) {$neighbours++;}}
    if($y != $size[1]-1) {if($cells[$x][$y+1]) {$neighbours++;}}
    if($x != $size[0]-1 && $y != 0) {if($cells[$x+1][$y-1]) {$neighbours++;}}
    if($x != $size[0]-1) {if($cells[$x+1][$y]) {$neighbours++;}}
    if($x != $size[0]-1 && $y != $size[1]-1) {if($cells[$x+1][$y+1]) {$neighbours++;}}

    return $neighbours;
}

/**
 * Return true if the tested cell will be a new alive cell
 *
 * @param array $cells  Cells grid
 * @param array $cell   Cell being tested
 * @param int   $neighbours Number of alive cells around the cell
 * @return bool true means true and false means false 👍
 */
function willBorn($cells, $cell, $neighbours) {
    $x = $cell[0];
    $y = $cell[1];

    return ($neighbours === 3 && !$cells[$x][$y]);
}

/**
 * Return true if the tested cell will be a fresh dead cell
 *
 * @param array $cells  Cells grid
 * @param array $cell   Cell being tested
 * @param int   $neighbours Number of alive cells around the cell
 * @return bool true means true and false means false 👍
 */
function willDie($cells, $cell, $neighbours) {
    $x = $cell[0];
    $y = $cell[1];

    return (($neighbours < 2 OR $neighbours > 3) && $cells[$x][$y]);
}

/**
 * Calculate new pending generation changes
 *
 * @param array $cells  Cells grid
 * @param array $size   Grid size table [x, y]
 * @return array    Pending actions
 */
function changes($cells, $size) {
    $pending = ['birth' => Array(), 'death' => Array()];

    for($x = 0; $x < $size[0]; $x++) {
        for($y = 0; $y < $size[1]; $y++) {
            $cell = [$x, $y];

            $neighbours = getNeighbours($cells, $size, $cell);

            if(willBorn($cells, $cell, $neighbours)) {array_push($pending['birth'], $cell);}
            if(willDie($cells, $cell, $neighbours)) {array_push($pending['death'], $cell);}
        }
    }

    return $pending;
}

/**
 * Apply pending changes
 *
 * @param array $cells  Cells grid
 * @param array $pending    Pending actions
 * @return array    Fresh updated cells grid
 */
function apply($cells, $pending) {
    foreach ($pending['birth'] as $cell) {
        $x = $cell[0];
        $y = $cell[1];

        $cells[$x][$y] = true;
    }

    foreach ($pending['death'] as $cell) {
        $x = $cell[0];
        $y = $cell[1];

        $cells[$x][$y] = false;
    }

    return $cells;
}

/**
 * Calculate and apply generation changes
 *
 * @param array $cells  Cells grid
 * @param array $size   Grid size table [x, y]
 * @return array    New generation cells grid
 */
function nextGen($cells, $size) {
    $pending = changes($cells, $size); // Calculate changes
    $cells = apply($cells, $pending); // Apply changes

    $_SESSION['current_generation']++; // Update current generation iterator

    return $cells;
}

if(!isset($_SESSION['cells'])) { // Init game if no grid in session
    $cells = init(Array(), $size);

    $_SESSION['cells'] = $cells;
    $_SESSION['size'] = $size;
    $_SESSION['current_generation'] = 0;
} else { // Otherwise, calculate new generation
    if(isset($generation) && @$generation > $_SESSION['current_generation']) {
        // Calculate to the "$generation" generation, but not if "$generation" is bigger than the current gen
        $cells = $_SESSION['cells'];
        $size = $_SESSION['size'];

        for($g = $_SESSION['current_generation']; $g < $generation; $g++) {
            $cells = nextGen($cells, $size);
        }
    } else {
        $cells = nextGen($_SESSION['cells'], $_SESSION['size']); // Update cells
    }

    $_SESSION['cells'] = $cells; // Updating cells
}
