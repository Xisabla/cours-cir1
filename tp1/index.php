<?php require('game.php'); ?>
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if(isset($sAuto)) { ?>
    <meta http-equiv="refresh" content="<?= $sAuto ?>; url=./index.php?auto=<?= $sAuto ?>" />
    <?php } ?>
    <title>Le jeu de la vie</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <h1>Le jeu de la vie</h1>

    <?php include('grid_template.html'); ?>
    <div id="actions">
        <p>
            Generation:
            <?= $_SESSION['current_generation']; ?>
                -
                <a href="./">Next generation</a>
                -
                <a href="?reset">Reset</a>
        </p>
        Auto refresh:
        <?php if(isset($sAuto)) { ?>
        <a href="./">Stop auto</a>
        <?php } else { ?>
        <form method="get">
            <input type="number" name="auto" value="1" min="0.1" max="10" step="0.1">
            <input type="submit" value="Let's go BB">
        </form>
        <?php } ?> -
        <form method="get">Calculate generation:
            <input type="number" name="generation" value="<?= $_SESSION['current_generation'] + 1; ?>"
                min="<?= $_SESSION['current_generation'] + 1; ?>" max="99999">
            <input type="submit" value="To the moon ! 🚀">
        </form>
    </div>
    <p>
        <?php if($debug) {var_dump($_SESSION);} ?>
    </p>
</body>

</html>
