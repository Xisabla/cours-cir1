<?php

if(!isset($_SESSION['date'])) {
    $_SESSION['date'] = strtotime('today');
}

function getToday($date, $iterator) {
    $day = dayOfMonth($date, $iterator);
    $today = date('Y-m-', $date);
    $today .= ($day < 10) ? '0' : '';
    $today .= $day;

    return $today;
}

function getMonthName($timestamp) {
    $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre'];
    $index = ltrim(date('m', $timestamp), '0') - 1;

    return $months[$index];
}

function getYear($timestamp) {
    return date('Y', $timestamp);
}

function monthStartDay($timestamp) {
    [$year, $month] = explode("-", date('Y-m', $timestamp));

    return date('D', mktime(0, 0, 0, $month, 1, $year));
}

function dayOfMonth($date, $iterator) {
    $days = [];
    $days['en'] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    $days['fr'] = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

    $start = monthStartDay($date);
    $current = $iterator - array_search($start, $days['en']) + 1;

    if($current < 1 || $current > date('t', $date)) {return null;}

    return $current;
}

function redirect($page = './', $message = '', $type = 'danger ') {
    if($message != '') {
        $_SESSION['error'] = $message;
        $_SESSION['alert_type'] = $type;
    }
    header('location: ' . $page);
    die();
}

function check_alert() {
    if(isset($_SESSION['error'])) {
        $message = $_SESSION['error'];
        $type = $_SESSION['alert_type'];

        echo renderAlert($message, $type);

        unset($_SESSION['error']);
        unset($_SESSION['alert_type']);
    }
}

function renderAlert($message, $type) {
    return '<div class="alert alert-'.$type.' alert-dismissible fade show" role="alert">
                '.$message.'
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>';
}

function renderDayCustomer($db, $date, $iterator, $user_id) {
    $n = 0;
    $day = dayOfMonth($date, $iterator);
    $today = getToday($date, $iterator);
    $html = '<div class="text-center">';
    $events = getEvents($db, $today);

    foreach ($events as $event) {
        if(!isEventFull($db, $event)) {
            if($n < 5) {
                $type = (userParticipateEvent($db, $event['id'], $user_id)) ? 'warning' : 'info';
                $html .= '<a href="?event=' . htmlspecialchars($event['id'], ENT_QUOTES) . '" class="btn btn-' . $type . ' btn-sm btn-block mt-1">' . htmlspecialchars($event['name'], ENT_QUOTES) . '</a>';
            }
            $n++;
        }
    }

    if($n > 5) {
        $html .= '<a href="?show=' . $today . '" class="btn btn-secondary btn-sm btn-block mt-1">Voir plus</a>';
    }

    if($n == 0) {
        $html = '<strong><i>Aucun évenement</i></strong>';
    }

    $html .= '</div>';

    if($day == null) {return '';}

    return '<p class="text-right text-muted text-italic"><em>' . $day . '</em></p>' . $html ;
}

function renderDayOrganizer($db, $date, $iterator, $organizer_id) {
    $day = dayOfMonth($date, $iterator);
    $today = getToday($date, $iterator);
    $html = '<div class="text-center">';
    $events = getEvents($db, $today, $organizer_id, 5);

    foreach ($events as $event) {
        $html .= '<a href="?event=' . htmlspecialchars($event['id'], ENT_QUOTES) . '" class="btn btn-info btn-sm btn-block mt-1">' . htmlspecialchars($event['name'], ENT_QUOTES) . '</a>';
    }

    if(nbEvents($db, $today, $organizer_id) > 5) {
        $html .= '<a href="?show=' . $today . '" class="btn btn-secondary btn-sm btn-block mt-1">Voir plus</a>';
    }

    if(nbEvents($db, $today, $organizer_id) == 0) {
        $html = '<strong><i>Aucun évenement</i></strong>';
    }

    $html .= '<a href="?add=' . $today . '" class="btn btn-success btn-sm btn-block mt-1">Ajouter un événement</a>';
    $html .= '</div>';

    if($day == null) {return '';}

    return '<p class="text-right text-muted text-italic"><em>' . $day . '</em></p>' . $html ;
}

function renderEventCustomer($db, $event, $user_id) {
    $btnFull = (isEventFull($db, $event)) ? ' disabled' : '';
    $full = (isEventFull($db, $event)) ? ' - <strong class="text-danger">Complet</strong>' : '';
    $participate = (userParticipateEvent($db, $event['id'], $user_id)) ?
        '<p class="text-center text-success"><strong>Vous participez à cet événement</strong></p><p class="text-right"><a href="./?cancel=' . htmlspecialchars($event['id'], ENT_QUOTES) . '" class="btn btn-danger">Annuler la participation à l\'événement</a></p>' :
        '<p class="text-right"><a href="./?reserve=' . htmlspecialchars($event['id'], ENT_QUOTES) . '" class="btn btn-success' . $btnFull . '">Participer à l\'événement</a></p>';

    return '    <div class="card bg-light mb-3">
        <div class="card-header"><h4>' . htmlspecialchars($event['name'], ENT_QUOTES) . '</h4></div>
        <div class="card-body">
            <p class="card-text"><strong>Participants:</strong> ' . eventParticipants($db, $event['id']) . '/' . htmlspecialchars($event['nb_place'], ENT_QUOTES) . ' ' . $full . '</p>
            <p class="card-text"><strong>Description:</strong> ' . htmlspecialchars($event['description'], ENT_QUOTES) . '</p>
            <p class="card-text">
                <strong>Début:</strong>' . htmlspecialchars($event['startdate'], ENT_QUOTES) . ' -
                <strong>Fin:</strong> ' . htmlspecialchars($event['enddate'], ENT_QUOTES) . '
            </p>
            ' . $participate . '
        </div>
    </div>';
}

function renderEventOrganizer($db, $event) {
    $full = (isEventFull($db, $event)) ? ' - <strong class="text-danger">Complet</strong>' : '';

    return '    <div class="card bg-light mb-3">
        <div class="card-header"><h4>' . htmlspecialchars($event['name'], ENT_QUOTES) . '</h4></div>
        <div class="card-body">
            <p class="card-text"><strong>Participants:</strong> ' . eventParticipants($db, $event['id']) . '/' . htmlspecialchars($event['nb_place'], ENT_QUOTES) . ' ' . $full . '</p>
            <p class="card-text"><strong>Description:</strong> ' . htmlspecialchars($event['description'], ENT_QUOTES) . '</p>
            <p class="card-text">
                <strong>Début:</strong>' . htmlspecialchars($event['startdate'], ENT_QUOTES) . ' -
                <strong>Fin:</strong> ' . htmlspecialchars($event['enddate'], ENT_QUOTES) . '
            </p>
            <p class="text-right"><a href="./?cancel=' . htmlspecialchars($event['id'], ENT_QUOTES) . '" class="btn btn-danger">Annuler l\'événement</a></p>
        </div>
    </div>';
}