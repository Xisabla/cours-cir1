<?php

/**
 * @return PDO
 */
function connectDB() {
    try {
        $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
        $db = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8', 'root', '020600', $opts);
    } catch (PDOException $e) {
        die('Erreur lors de la connection à la base de donnée : ' . $e->getMessage());
    }

    return $db;
}