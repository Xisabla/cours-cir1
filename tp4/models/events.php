<?php

/**
 * @param PDO $db   PDO Object
 * @param string $day   Day of the event AAAA-MM-DD
 * @param string $organizer_id  The id of the events organizer (null for none)
 * @param int $limit    Max number of events (null for none)
 * @return array    Events table
 */
function getEvents(PDO $db, $day, $organizer_id = null, $limit = null) {
    $sql = 'SELECT * FROM events WHERE DATE(startdate) = :date';
    if($organizer_id != null) {$sql .= ' AND organizer_id = :organizer_id';}
    if($limit != null) {$sql .= ' LIMIT ' . $limit;}

    $data = [':date' => $day];
    if($organizer_id != null) {$data[':organizer_id'] = $organizer_id;}

    $req = $db->prepare($sql);
    $req->execute($data);

    return $req->fetchAll();
}

/**
 * @param PDO $db   PDO Object
 * @param string $event_id id of the event
 * @return mixed    Event information
 */
function getEvent(PDO $db, $event_id) {
    $req = $db->prepare('SELECT * FROM events WHERE id = :id');
    $req->execute(array(
        ':id' => $event_id
    ));

    return $req->fetch();
}

/**
 * @param PDO $db PDO Object
 * @param string $name Name of the event
 * @param string $description  Description of the event
 * @param string $startdate    Starting datetime of the event (AAAA-MM-DD HH:MM:SS)
 * @param string $enddate  Ending datetime of the event (AAAA-MM-DD HH:MM:SS)
 * @param string $organizer_id id of the organizer of the event
 * @param string $nb_place Number of places for the event
 * @return bool Success of the SQL request
 */
function addEvent(PDO $db, $name, $description, $startdate, $enddate, $organizer_id, $nb_place) {
    $req = $db->prepare('INSERT INTO events (name, description, startdate, enddate, organizer_id, nb_place) VALUE (:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');
    $rep = $req->execute(array(
        ':name' => $name,
        ':description' => $description,
        ':startdate' => $startdate,
        ':enddate' => $enddate,
        ':organizer_id' => $organizer_id,
        ':nb_place' => $nb_place
    ));

    return $rep;
}

/**
 * @param PDO $db   PDO Oject
 * @param string $event_id id of the event
 * @return bool Success of the SQL request
 */
function deleteEvent(PDO $db, $event_id) {
    $reqA = $db->prepare('DELETE FROM user_participates_events WHERE id_event = :id_event');
    $reqB = $db->prepare('DELETE FROM events WHERE id = :id');
    if($reqA->execute(array(':id_event' => $event_id))) {
        return $reqB->execute(array(':id' => $event_id));
    }

    return false;
}

/**
 * @param PDO $db   PDO Object
 * @param string $event_id id of the event
 * @return int  Number of participants of the event
 */
function eventParticipants(PDO $db, $event_id) {
    $req = $db->prepare('SELECT COUNT(*) FROM user_participates_events WHERE id_event = :id_event');
    $req->execute(array(
        ':id_event' => $event_id
    ));

    return $req->fetch()[0];
}

/**
 * @param PDO $db   PDO Object
 * @param array $event    id of the event
 * @return bool is full or not
 */
function isEventFull(PDO $db, $event) {
    $event_id = $event['id'];
    $nb_place = $event['nb_place'];

    return (eventParticipants($db, $event_id) == $nb_place);
}

/**
 * @param PDO $db   PDO Object
 * @param string $event_id  id of the event
 * @param string $user_id   id of the user
 * @return bool True of the user participate to the event (Pff... I mean, it's obvious)
 */
function userParticipateEvent(PDO $db, $event_id, $user_id) {
    $req = $db->prepare('SELECT * FROM events e INNER JOIN user_participates_events u WHERE e.id = u.id_event AND e.id = :event_id AND u.id_participant = :user_id');
    $req->execute(array(
        ':event_id' => $event_id,
        ':user_id' => $user_id
    ));

    return (bool) $req->rowCount();
}

/**
 * @param PDO $db   PDO Object
 * @param $event_id id of the event
 * @param $user_id  id of the user
 * @return bool Success of the SQL request
 */
function userRegisterEvent(PDO $db, $event_id, $user_id) {
    $req = $db->prepare('INSERT INTO user_participates_events SET id_event = :id_event, id_participant = :id_user');
    return $req->execute(array(
        ':id_event' => $event_id,
        ':id_user' => $user_id
    ));
}

/**
 * @param PDO $db   PDO Object
 * @param $event_id id of the event
 * @param $user_id  id of the user
 * @return bool Success of the SQL request
 */
function userCancelEvent(PDO $db, $event_id, $user_id) {
    $req = $db->prepare('DELETE FROM user_participates_events WHERE id_event = :id_event AND id_participant = :id_user');
    return $req->execute(array(
        ':id_event' => $event_id,
        ':id_user' => $user_id
    ));
}

/**
 * @param PDO $db   PDO Object
 * @param string $day  Day to check
 * @param string $organizer_id Id of the organizer of the events
 * @return int  Number of event for the day
 */
function nbEvents(PDO $db, $day, $organizer_id)
{
    $req = $db->prepare('SELECT COUNT(*) FROM events WHERE DATE(startdate) = :date AND organizer_id = :organizer_id');
    $req->execute(array(
        ':date' => $day,
        ':organizer_id' => $organizer_id
    ));

    return $req->fetch()[0];
}
