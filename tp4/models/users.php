<?php

function is_connected() {
    if(isset($_SESSION['id'])) {
        return true;
    } else {
        return false;
    }
}

function checkCredentials(PDO $db, $username, $password) {
    $req = $db->prepare('SELECT * FROM Users WHERE login = :login');
    $req->execute(array(
        ':login' =>  $username
    ));

    if($req->rowCount() < 1) {return false;}

    $userData = $req->fetch();
    if(password_verify($password, $userData['password'])) {
        $_SESSION['id'] = $userData['id'];
        return true;
    }

    return false;
}

function getUser(PDO $db, $id) {
    $req = $db->prepare('SELECT * FROM Users WHERE id = :id');
    $req->execute(array(
        ':id' => $id
    ));

    return $req->fetch();
}