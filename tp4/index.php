<?php

session_start();

// Loading Helpers
require('./helpers.php');

// Loading Models
require('./models/bdd.php');
require('./models/events.php');
require('./models/users.php');

// Logout
if(is_connected() && isset($_GET['logout'])) {session_destroy(); redirect('./');}

$db = connectDB();

// Meta, title & css for views
require('./views/head.php');

// Basic routes: organizer, customer or login
if(is_connected()) {
    $user= getUser($db, $_SESSION['id']);

    if($user['rank'] == 'ORGANIZER') {require('./controllers/organizer.php');}
    if($user['rank'] == 'CUSTOMER') {require('./controllers/customer.php');}
} else {
    require('./controllers/login.php');
}

// JS scripts
require('./views/foot.php');