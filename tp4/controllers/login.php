<?php

if(isset($_GET['login'])) {
    $username = filter_input(INPUT_POST, 'username');
    $password = filter_input(INPUT_POST, 'password');

    if(checkCredentials($db, $username, $password)) {
        redirect('./');
    } else {
        redirect('./','<strong>Bad Credentials</strong> - You tried to sign in with a wrong username or password !',  'danger');
    }
} else {
    require('./views/login.php');
}