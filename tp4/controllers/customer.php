<?php

/**
 * @var array $user User information
 */

$date = $_SESSION['date'];

if(isset($_GET['previous'])) {
    $_SESSION['date'] = strtotime('-1 month', $date);
    redirect('./');
}

if(isset($_GET['next'])) {
    $_SESSION['date'] = strtotime('+1 month', $date);
    redirect('./');
}

// Advanced routes: show, event, reserve, cancel, default
if(isset($_GET['show'])) {
    $day = filter_input(INPUT_GET, 'show');
    $events = getEvents($db, $day);

    if($events == false) {redirect('./', 'Aucun événement pour cette date !', 'danger');}

    require('./views/allEventsCustomer.php');
} else if(isset($_GET['event'])) {
    $event_id = filter_input(INPUT_GET, 'event');
    $event = getEvent($db, $event_id);

    if($event == false) {redirect('./', 'Cet événement n\'existe pas (ou plus) !', 'danger');}

    require('./views/showEventCustomer.php');
} else if(isset($_GET['reserve'])) {
    $event_id = filter_input(INPUT_GET, 'reserve');
    $event = getEvent($db, $event_id);

    if($event == false) {redirect('./', 'Cet événement n\'existe pas (ou plus) !', 'danger');}
    if(isEventFull($db, $event)) {redirect('./', 'Cet événement est plein !', 'danger');}

    if(userRegisterEvent($db, $event['id'], $user['id'])) {
        redirect('./', 'Vous participez désormais à l\'événement', 'success');
    } else {
        redirect('./', 'Impossible de participer à cet événement', 'danger');
    }
} else if(isset($_GET['cancel'])) {
    $event_id = filter_input(INPUT_GET, 'cancel');
    $event = getEvent($db, $event_id);

    if($event == false) {redirect('./', 'Cet événement n\'existe pas (ou plus) !', 'danger');}
    if(!userParticipateEvent($db, $event_id, $user['id'])) {redirect('./', 'Vous ne participez déjà pas à cet événement !', 'danger');}

    if(userCancelEvent($db, $event['id'], $user['id'])) {
        redirect('./', 'Vous ne participez plus à l\'événement', 'success');
    } else {
        redirect('./', 'Impossible d\'annuler votre participation à cet événement', 'danger');
    }
} else { // Calendar
    require('./views/calendarCustomer.php');
}