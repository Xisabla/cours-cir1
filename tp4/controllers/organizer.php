<?php
/**
 * @var array $user User information
 */

$date = $_SESSION['date'];

if(isset($_GET['previous'])) {
    $_SESSION['date'] = strtotime('-1 month', $date);
    redirect('./');
}

if(isset($_GET['next'])) {
    $_SESSION['date'] = strtotime('+1 month', $date);
    redirect('./');
}

// Advanced routes: show (show events details for a date), event (show event details for an id), cancel (delete event), add (add an event), default (calendar)
if(isset($_GET['show'])) {  // Show all events for the date
    $day = filter_input(INPUT_GET, 'show');
    $events = getEvents($db, $day, $user['id']);

    if($events == false) {redirect('./', 'Aucun événement pour cette date !', 'danger');}

    require('./views/allEventsOrganizer.php');
} else if(isset($_GET['event'])) { // Show the event details
    $event_id = filter_input(INPUT_GET, 'event');
    $event = getEvent($db, $event_id);

    if($event == false) {redirect('./', 'Cet événement n\'existe pas (ou plus) !', 'danger');}

    require('./views/showEventOrganizer.php');
} else if(isset($_GET['cancel'])) { // Cancel an event
    $event_id = filter_input(INPUT_GET, 'cancel');
    $event = getEvent($db, $event_id);

    if($event == false) {redirect('./', 'Cet événement n\'existe pas (ou plus) !', 'danger');}

    if(deleteEvent($db, $event_id)) {
        redirect('./', 'Événement annulé', 'success');
    } else {
        redirect('./?cancel=' . $event_id, 'Erreur lors de l\'annulation de l\'événement', 'danger');
    }
} else if(isset($_GET['add'])) { // Add an event
    if(isset($_GET['submitted'])) { // Submit add
        $name = filter_input(INPUT_POST, 'name');
        $description = filter_input(INPUT_POST, 'description');
        $startdate = str_replace('T', ' ', filter_input(INPUT_POST, 'startdate')) . ':00';
        $enddate = str_replace('T', ' ', filter_input(INPUT_POST, 'enddate')) . ':00';
        $organizer_id = $user['id'];
        $nb_place = filter_input(INPUT_POST, 'nb_place');

        if(addEvent($db, $name, $description, $startdate, $enddate, $organizer_id, $nb_place)) {
            redirect('./', 'Événement créé', 'success');
        } else {
            redirect('./', 'Impossible de créer l\'événement', 'danger');
        }
    } else { // Go to add form
        $day = filter_input(INPUT_GET, 'add');

        require('./views/addEventOrganizer.php');
    }
} else { // Calendar
    require('./views/calendarOrganizer.php');
}
