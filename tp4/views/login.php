<div class="container text-center mt-5">
    <h1>Connexion</h1>
    <form class="form-signin mt-5" method="post" action="./?login">
        <label for="username" class="sr-only">Nom d'utilisateur</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="Nom d'utilisateur" required autofocus>
        <br>
        <label for="password" class="sr-only">Mot de passe</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe" required>
        <br>
        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Se connecter">
    </form>
</div>
