<?php
/**
 * @var string $day Day of the events
 * @var array $events   Events information
 * @var array $user User information
 */
?>

<div class="container">
    <div id="backToHome">
        <a href="./"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
    </div>

    <h1 class="text-center py-5">Événements du <?= htmlspecialchars(date_format(date_create($day), 'd/m/Y'), ENT_QUOTES); ?></h1>

    <?php foreach ($events as $event): ?>
        <?= renderEventCustomer($db, $event, $user['id']); ?>
    <?php endforeach; ?>
</div>

