<?php
/**
 * @var array $event   Event information
 * @var array $user User information
 */
?>

<div class="container">
    <div id="backToHome">
        <a href="./"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
    </div>

    <h1 class="text-center py-5">Événement (<em><?= htmlspecialchars($event['id'], ENT_QUOTES) ?></em>)   du <?= htmlspecialchars(date_format(date_create($event['startdate']), 'd/m/Y'), ENT_QUOTES); ?></h1>
    <?= renderEventOrganizer($db, $event); ?>
</div>

