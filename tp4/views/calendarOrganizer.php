<?php
/**
 * @var array $user User information
 */
?>

<div>
    <div id="backToHome">
        <a href="./?logout"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
    </div>

    <div class="row container-fluid text-center my-5">
        <div class="col"><a href="./?previous" class="btn btn-primary">Précédent</a></div>
        <div class="col">
            <h1><?= htmlspecialchars(getMonthName($date), ENT_QUOTES); ?> <?= htmlspecialchars(getYear($date), ENT_QUOTES); ?></h1>
        </div>
        <div class="col"><a href="./?next" class="btn btn-primary">Suivant</a></div>
    </div>

    <table class="table table-striped">
        <thead class="text-center">
            <tr>
                <th>Lundi</th>
                <th>Mardi</th>
                <th>Mercredi</th>
                <th>Jeudi</th>
                <th>Vendredi</th>
                <th>Samedi</th>
                <th>Dimanche</th>
            </tr>
        </thead>
        <tbody>
            <?php for($i = 0; $i < 6; $i++): ?>
            <tr>
            <?php for($j = 0; $j < 7; $j++): ?>
                <td>
                    <?= renderDayOrganizer($db, $date, $i*7 + $j, $user['id']); ?>
                </td>
            <?php endfor; ?>
            </tr>
            <?php endfor; ?>
        </tbody>
    </table>

</div>