<!DOCTYPE html>
<html lang="fr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
    <!-- Font Awesome WOW -->
    <link href="https://use.fontawesome.com/releases/v5.0.4/css/all.css" rel="stylesheet">

    <title>Wow such chat</title>
</head>
<body>
    <div class="jumbotron pb-1 text-center">
        <div class="container-fluid" style="background: url('./public/img/doge-banner.jpeg') no-repeat center; height: 100px"></div>
        <h1 class="display-1"><img src="./public/img/doge.png" width="200">Wow such chat<img src="./public/img/doge.png" width="200"></h1>

        <div id="messages" class="text-left bg-light px-3 py-2">
            <div class="message my-3">
                <h5 class="display-5 text-center text-primary">Bienvenue sur ce magnifique chat, restez polis, gentils et doux !</h5>
            </div>
            <?php foreach ($messages as $message) { ?>
            <div class="message my-2">
                <?php if(substr($message['message'],  0, 3) == '/me') { ?>
                <?= meMessage($message) ?>
                <?php } else { ?>
                <strong><?= $message['author'] ?></strong> <span class="font-weight-light">(<?= $message['date'] ?>)</span>: <?= $message['message'] ?>
                <?php } ?>
            </div>
            <?php } ?>
        </div>

        <form class="mt-2" action="./" method="POST">
            <div class="row">
                <div class="col-2">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Nom d'utilisateur" <?php if(isset($_POST['username'])) {echo 'value="' . $_POST['username'] . '"';} ?>>
                    </div>
                </div>
                <div class="col-8">
                    <div class="form-group">
                        <input type="text" class="form-control" id="message" name="message" placeholder="Message">
                    </div>
                </div>
                <div class="col-2">
                    <button type="submit" class="btn btn-primary">Send Message</button>
                </div>
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('input#message').focus();
        })
    </script>
</body>
</html>