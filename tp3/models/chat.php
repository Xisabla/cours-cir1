<?php

/**
 * Initialize bdd connexion, die on failure
 * @return PDO  BDD Object
 */
function initBDD() {
    try {
        $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
        $bdd = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', '020600', $opts);

        return $bdd;
    } catch (PDOException $e) {
        die('Erreur lors de la connection à la base de donnée : ' . $e->getMessage());
    }
}

/**
 * Get the messages stored
 * @param PDO $bdd  BDD Object
 * @return array    Messages
 */
function getMessages($bdd) : array {
    return $bdd->query('SELECT * FROM messages')->fetchAll();
}

/**
 * @param PDO $bdd  BDD Object
 * @param string $message   Message to send
 * @param string $author    Message's author
 * @param string $date  Message's date
 * @return bool SQL return status
 */
function sendMessage($bdd, $message, $author, $date) {
    $req = $bdd->prepare('INSERT INTO messages (message, author, date) VALUES  (:message, :author, :date)');
    return $req->execute(array(
        ':message' => $message,
        ':author' => $author,
        ':date' => $date
    ));
}

/**
 * @param PDO $bdd  BDD Object
 * @param string $author    Author of the messages to ban
 * @param string $reason    Reason of the ban
 * @return bool SQL return status
 */
function banMessages($bdd, $author, $reason) {
    $message = '<em class="text-danger">Ce message a été banni pour la raison suivante: <strong>' . $reason . '</strong></em>';
    $req = $bdd->prepare('UPDATE messages SET message = :message WHERE author = :author');
    return $req->execute(array(
        ':message' => $message,
        ':author' => $author
    ));
}

/**
 * Put HTML around the message of the /me
 * @param array $message   Message
 * @return string   HTML
 */
function meMessage($message) {
    return '<em class="text-muted"><strong>' . $message['author']. '</strong> ' . substr($message['message'], 4, strlen($message['message'])) . '</em>';
}

/**
 * Clear messages from a user
 * @param PDO $bdd  BDD Object
 * @param string $author    Messages' author (* for all messages)
 * @return PDOStatement|bool    SQL Query or SQL return status
 */
function clearMessages($bdd, $author) {
    if($author == '*') {
        return $bdd->query('DELETE FROM messages WHERE 1=1');
    } else {
        $req = $bdd->prepare('DELETE FROM messages WHERE author = :author');
        return $req->execute(array(
            ':author' => $author
        ));
    }
}