<?php

require('./models/chat.php');


$bdd = initBDD();

if(isset($_POST['username']) && isset($_POST['message'])) {
    $message = $_POST['message'];
    $author = $_POST['username'];
    $date = date("Y-m-d H:i:s");

    $explodedMessage = explode(' ', $message);

    if($explodedMessage[0] == '/ban') {
        banMessages($bdd, $explodedMessage[1], implode(' ', array_slice($explodedMessage, 2)));
    } elseif($explodedMessage[0] == '/clear') {
        $author = (isset($explodedMessage[1])) ? $explodedMessage[1] : '*';
        clearMessages($bdd, $author);
    } else {
        sendMessage($bdd, $message, $author, $date);
    }
}

$messages = getMessages($bdd);

require('./views/chat.php');