<?php

define('TARGET_DIRECTORY', './uploads/');
define('CSV_DB', './mesproduits.csv');

require('./_includes/init.php');

if(isset($_GET)) {
    if(isset($_GET['nom_produit'])) {
        $nom_produit = htmlspecialchars($_GET['nom_produit'], ENT_QUOTES);

        if(!preg_match("/^[a-zA-Z0-9 ]{2,30}$/", $nom_produit)) {
            redirect("Le pproduit n'est pas valide", './liste_produits.php', 'danger');
        }

        $produits = array();

        $fp = fopen(CSV_DB, 'r');
        $found = 0;
        while (($produit = fgetcsv($fp, 0, ';')) !== false) {
            if($produit[0] == $nom_produit) {
                $found = 1;
                if($produit[2] <= 0) {
                    redirect("Le produit " . $nom_produit . " n'est plus disponible en stock", './liste_produits.php', 'danger');
                }
                $produit[2]--;
            }
            $produits[] = $produit;
        }

        fclose($fp);

        $fp = fopen(CSV_DB, 'w');

        foreach ($produits as $produit) {
            if(!fputcsv($fp, $produit, ';')) {
                fclose($fp);
                redirect("Une erreur est survenue lors de l'enregistrement du produit.", './liste_produits.php', 'danger');
            }
        }

        fclose($fp);
        if(!$found) {
            redirect("Le produit " . $nom_produit . " n'existe pas.", './liste_produits.php', 'danger');
        }

        redirect("Produit correctement vendu", './liste_produits.php', 'success');
    }
}