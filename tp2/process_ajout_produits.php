<?php

require('_includes/init.php');

define('TARGET_DIRECTORY', './uploads/');
define('CSV_DB', './mesproduits.csv');

if(!empty($_FILES['photo_produit'])) {
    move_uploaded_file($_FILES['photo_produit']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo_produit']['name']);
}

if(isset($_POST['nom_produit']) && isset($_POST['prix_produit']) && isset($_POST['nombre_produit'])) {
    $nom_produit = htmlspecialchars($_POST['nom_produit'], ENT_QUOTES);
    $prix_produit = htmlspecialchars($_POST['prix_produit'], ENT_QUOTES);
    $nombre_produit = htmlspecialchars($_POST['nombre_produit'], ENT_QUOTES);

    if(!preg_match("/^[a-zA-Z0-9 ]{2,30}$/", $nom_produit)) {
        redirect("Le nom du produit n'est pas au bon format", './ajout_produits.php', 'danger');
    }

    if(!preg_match("/^[^-]\s*[0-9]*.?[0-9]{0,2}+$/", $prix_produit)) {
        redirect("Le prix du produit n'est pas au bon format", './ajout_produits.php');
    }

    if(!preg_match("/^[0-9]{0,9}$/", $nombre_produit)) {
        redirect("Le nombre de produits entré est invalide", './ajout_produits.php', 'danger');
    }

    $produit = array($nom_produit, $prix_produit, $nombre_produit, TARGET_DIRECTORY . $_FILES['photo_produit']['name']);

    $fp = fopen(CSV_DB, 'a');
    $rput = fputcsv($fp, $produit, ';');
    fclose($fp);

    if(!$rput) {
        redirect("Une erreur est survenue lors de l'enregistrement du produit", './ajout_produits.php', 'danger');
    }

    redirect("Produit correctement enregistré", './ajout_produits.php', 'success');
} else {
    redirect("Formulaire incorrectement rempli", './ajout_produits.php', 'danger');
}