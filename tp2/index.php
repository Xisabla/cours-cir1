<?php

require('./_includes/init.php');
require('./_includes/head.php');

check_alert();

?>

    <div class="container">
        <div class="jumbotron mt-3 text-center">
            <h1 class="display-1">🍑 Adolf l'épicier 🍑</h1>
            <div class="container mt-5">
                <div class="row">
                    <div class="col">
                        <a href="./ajout_produits.php" class="btn btn-light">
                            <img src="./img/ajouter.png">
                            <br>
                            <h6 class="display-6 mt-2">Ajouter des produits</h6>
                        </a>
                    </div>
                    <div class="col">
                        <a href="./liste_produits.php" class="btn btn-light">
                            <img src="./img/lister.png">
                            <br>
                            <h6 class="display-6 mt-2">Lister les produits</h6>
                        </a>
                    </div>
                </div>
            </div>
        </div>

<?php require('./_includes/foot.php'); ?>