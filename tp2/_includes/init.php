<?php

session_start();

function redirect($message, $page, $type = 'danger ') {
    $_SESSION['error'] = $message;
    $_SESSION['alert_type'] = $type;
    header('location: ' . $page);
    die();
}

function check_alert() {
    if(isset($_SESSION['error'])) {
        $message = $_SESSION['error'];
        $type = $_SESSION['alert_type']; ?>
        <div class="alert alert-<?= $type ?> alert-dismissible fade show" role="alert">
            <?= $message ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php unset($_SESSION['error']);
    }
}