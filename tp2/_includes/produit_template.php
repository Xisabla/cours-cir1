<div class="col">
    <div class="card">
        <h3 class="card-header"><?= $produit[0] ?></h3>
        <div class="card-body">
            <img src="<?= $produit[3] ?>" height="200px" width="200px">
        </div>
        <div class="card-body">
            <p>Prix: <?= $produit[1] ?>€</p>
            <p>En stock: <?= $produit[2] ?></p>
            <a href="vendre_produit.php?nom_produit=<?= $produit[0] ?>" class="btn btn-success <?= ($produit[2] <= 0) ? 'disabled' : '' ?>"><?= ($produit[2] <= 0) ? 'En rupture' : '+1 Vendu' ?></a>
        </div>
    </div>
</div>