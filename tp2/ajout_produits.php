<?php

require('./_includes/init.php');
require('./_includes/head.php');

check_alert();

?>

    <div id="backToHome">
        <a href="./index.php"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
    </div>

    <div class="container">
        <div class="jumbotron mt-3 text-center">
            <h4 class="display-4">Ajouter un produit</h4>

            <form class="mt-5 text-left" action="./process_ajout_produits.php" method="POST" enctype="multipart/form-data">
                <fieldset id="produit">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="nom_produit">Nom du produit</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nom_produit" name="nom_produit" placeholder="Petits fours">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label" for="photo_produit">Photo du produit</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" id="photo_produit" name="photo_produit">
                        </div>
                    </div>
                </fieldset>
                <fieldset id="informations">
                    <div class="form-group row">
                        <label for="prix_produit" class="col-sm-2 col-form-label">Prix du produit</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="prix_produit" name="prix_produit" placeholder="Trop cher pour toi">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="nombre_produit" class="col-sm-2 col-form-label">Nombre de produit disponibles</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="nombre_produit" name="nombre_produit" placeholder="42">
                        </div>
                    </div>
                </fieldset>

                <div class="text-center">
                    <input type="submit">
                </div>
            </form>
        </div>
    </div>

<?php require('./_includes/foot.php'); ?>