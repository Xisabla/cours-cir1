<?php

define('TARGET_DIRECTORY', './uploads/');
define('CSV_DB', './mesproduits.csv');

require('./_includes/init.php');
require('./_includes/head.php');

check_alert();

?>

    <div id="backToHome">
        <a href="./index.php"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
    </div>

    <div class="container">
        <div class="jumbotron mt-3 text-center">
            <h4 class="display-4 mb-3">Liste des produits</h4>
            <?php

            $current = 0;
            $fp = fopen(CSV_DB, 'r');
            while (($produit = fgetcsv($fp, 0, ';')) !== false) {
                if(($current % 3 == 0)) {echo '<div class="row my-3">'; $divOpen = true;}
                include('./_includes/produit_template.php');
                if(($current % 3 == 2)) {echo '</div>'; $divOpen = false;}
                $current++;
            }
            if($divOpen) {echo '</div>'; $divOpen = false;}

            ?>

        </div>
    </div>

<?php require('./_includes/foot.php'); ?>